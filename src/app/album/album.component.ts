import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import {DataSource} from '@angular/cdk/collections';
import { Album } from '../models/album.model';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  dataSource = new UserDataSource(this.userService);
  displayedColumns = ['Userid', 'id', 'title'];
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

}
export class UserDataSource extends DataSource<any> {
  constructor(private userService: UserService) {
    super();
  }
  connect(): Observable<Album[]> {
    return this.userService.getUser1();
  }
  disconnect() {}
}
