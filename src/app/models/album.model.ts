export interface Album {
  Userid: string;
  id: string;
  title: string;
}
