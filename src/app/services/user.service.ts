import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '../models/user.model';
import { Album } from '../models/album.model';

@Injectable()
export class UserService {
  private serviceUrl = ' https://jsonplaceholder.typicode.com/users';
  private serviceUrl1 = 'https://jsonplaceholder.typicode.com/albums';

  constructor(private http: HttpClient) { }

  getUser(): Observable<User[]>{
    return this.http.get<User[]>(this.serviceUrl);
   }

   getUser1(): Observable<Album[]>{
    return this.http.get<Album[]>(this.serviceUrl1);
   }
}

